<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'flyingcursor_farmworld' );

/** Database username */
define( 'DB_USER', 'flyingcursor_farmworld_user' );

/** Database password */
define( 'DB_PASSWORD', 'R27@*!H338$DV38Rb9' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{lYR;~Zy5GUC&B[/fu9X`nK:R`F@dhk0oMCeg&w{2S^h7%YgG;`t-v9Xj9z1hqD5' );
define( 'SECURE_AUTH_KEY',  '~a9r7SnV+(xgG)t-]owW)t|Q|X|i$m ^sb_(hELfM bW=(wHp0%skN[W~=!d^}Z1' );
define( 'LOGGED_IN_KEY',    'l;..U00!SbQq;NQ4${F,BMhV0~o+m%UAjE0??n|E?rF~O`.0teVYj}2^;O2XD:Xa' );
define( 'NONCE_KEY',        'q[,x|=(ph8>lXI]kaR$hqow:T6$Jo/U+<o =JT&E~5tp.Q(}Y{,#ZPm`T<cM]rY@' );
define( 'AUTH_SALT',        '?%EAvjUm&<#&AAqMV3r&lxrPNMP4:(!N9pLNpTvPfw#HLJes#Zz]3r_A;!8k4|WU' );
define( 'SECURE_AUTH_SALT', 'WX3BtK#(]9M_#kYX%Li!s{iPQg/6Ra<F>bSz;`Td1*A :KO(N5tdj3?bJR!P-:3m' );
define( 'LOGGED_IN_SALT',   '!YggW}R`;mkoTwKCm?acPFqtH7MZ/_@g&3*+BYPg:wQ,Q;+0)F@gg=@bMw5gh}yr' );
define( 'NONCE_SALT',       'UxqCLY_q)3-~=Wjc52}a8v&@8=cR `>z|e!>mzYVs{d84)#orP0/}Y:MgE|o3_G@' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);
if ( ! defined( 'COOKIEHASH' ) ) {
    $siteurl = 'https://farmworld.flyingcursor.com/';
    if ( $siteurl ) {
        define( 'COOKIEHASH', md5( $siteurl ) );
    }
}
if ( ! defined( 'USER_COOKIE' ) ) {
    define( 'USER_COOKIE', 'farmworlduser_' . COOKIEHASH );
}
/**
 * @since 2.0.0
 */
if ( ! defined( 'PASS_COOKIE' ) ) {
    define( 'PASS_COOKIE', 'farmworldpass_' . COOKIEHASH );
}
/**
 * @since 2.5.0
 */
if ( ! defined( 'AUTH_COOKIE' ) ) {
    define( 'AUTH_COOKIE', 'farmworld_' . COOKIEHASH );
}
/**
 * @since 2.6.0
 */
if ( ! defined( 'SECURE_AUTH_COOKIE' ) ) {
    define( 'SECURE_AUTH_COOKIE', 'farmworld_sec_' . COOKIEHASH );
}
/**
 * @since 2.6.0
 */
if ( ! defined( 'LOGGED_IN_COOKIE' ) ) {
    define( 'LOGGED_IN_COOKIE', 'farmworld_logged_in_' . COOKIEHASH );
}
/**
 * @since 2.3.0
 */
if ( ! defined( 'TEST_COOKIE' ) ) {
    define( 'TEST_COOKIE', 'farmworld_test_cookie' );
}
if ( ! defined( 'RECOVERY_MODE_COOKIE' ) ) {
    /**
     * @since 5.2.0
     */
    define( 'RECOVERY_MODE_COOKIE', 'farmworld_rec_' . COOKIEHASH );
}



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
