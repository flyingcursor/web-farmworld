<?php

/**
 * Plugin Name: FC Custom Related Posts
 * Description: Retrieves related posts based on current tags and a dynamically passed category ID.
 * Version: 1.0.0
 * Author: Your Name
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

// Add a custom function to fetch related posts
function custom_get_related_posts($category_id)
{

    $tags = get_the_tags();  // Get tags of the current post

    $current_post_id = get_queried_object_id(); // Retrieve the ID of the current post

    $args = array(
        'post_type' => 'post',
        'category__in' => array($category_id), // Replace $category_id with the desired category ID
        'tag' => $tags[0]->slug, // Assuming 'your-tag-slug' is the tag slug
        'post__not_in' => array($current_post_id),
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'feature_post',
                'value' => '1', // Show posts with 'feature_post' set to false (0)
                'compare' => '!='
            )
        )
    );

    $query = new WP_Query($args);
    if ($query->have_posts()) {
        $category = get_the_category($current_post_id); // Get the post's categories
        if (!empty($category)) {
            $category_slug = $category[0]->slug; // Get the slug of the first category
            $category_name = $category[0]->name;
        }


        ?>
        <div class="row my-5 owl-carousel fc-story-card-slider">
            <?php
            while ($query->have_posts()) {
                $query->the_post();
                $related_post_id = get_the_ID();
                $country_name = get_field('country_name', $related_post_id);
                ?>
                <div class="col-12 col-lg-4 w-100">
                    <div class="card fc-story-card">
                        <div class="image-thumbnail">
                            <?php
                            if (has_post_thumbnail()) {
                                ?>
                                <a href="<?php the_permalink(); ?>">
                                    <?php
                                    the_post_thumbnail('medium', array('class' => 'img-fluid w-100'));
                                    ?>
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="fc-story-tag mt-4">
                            <h4><span><?php echo $category_name; ?></span> <?php echo $country_name; ?></h4>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                            <?php the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>">read more</a>
                        </div>
                    </div>
                </div>
                <?php
                // ...
            }
            ?>
        </div>
        <?php
    } else {
        ?>
        <p>No posts found!</p>
        <?php


        wp_reset_postdata();  // Reset the query
    }
}
