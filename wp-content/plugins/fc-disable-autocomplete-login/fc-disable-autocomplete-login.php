<?php
/*
Plugin Name: FC Disable Autocomplete on Login Page
Description: Disables autocomplete for username and password fields on wp-login.php.
Version: 1.0
Author: Flying Cursor
*/

defined('ABSPATH') or die('No script kiddies please!'); // Prevent direct access

function disable_autocomplete_login() {
    if (is_user_logged_in()) return; // Only apply to non-logged in users

    echo '<script>
        document.addEventListener("DOMContentLoaded", function() {
            document.getElementById("user_login").setAttribute("autocomplete", "off");
            document.getElementById("user_pass").setAttribute("autocomplete", "off");
        });
    </script>';
}

add_action('login_enqueue_scripts', 'disable_autocomplete_login');

