<?php

/**
 * Plugin Name: FC Embed Plugin
 * Plugin URI:  https://example.com
 * Description: A custom plugin to modify embed blocks.
 * Version:     1.0.0
 * Author:      Your Name
 * Author URI:  https://example.com
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

function parse_embed_block($content)
{
    if ($content) {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        // Find embed blocks in the content
        $embed_blocks = $dom->getElementsByTagName('figure');

        foreach ($embed_blocks as $embed_block) {
            // Extract the embedded content URL if an iframe exists
            $iframes = $embed_block->getElementsByTagName('iframe');
            if ($iframes->length > 0) {
                $iframe = $iframes->item(0);
                $embed_url = $iframe->getAttribute('src');

                // Modify the embed code or perform any desired operations
                // For example, you can update the attributes or wrap the embed code with additional HTML

                // Update the embed URL or perform any further processing
                // $embed_url = my_custom_function($embed_url);

                // Create a new wrapper div for the modified embed code
                $wrapper_div = $dom->createElement('div');
                $wrapper_div->setAttribute('class', 'ratio ratio-16x9');

                // Create a new iframe element with the modified embed code
                $new_iframe = $dom->createElement('iframe');
                $new_iframe->setAttribute('class', 'embed-responsive-item');
                $new_iframe->setAttribute('src', $embed_url);

                // Append the new iframe to the wrapper div
                $wrapper_div->appendChild($new_iframe);

                // Replace the embed block with the modified embed code
                $embed_block->parentNode->replaceChild($wrapper_div, $embed_block);
            }
        }

        return $dom->saveHTML();
    }
}

add_filter('the_content', 'parse_embed_block');
