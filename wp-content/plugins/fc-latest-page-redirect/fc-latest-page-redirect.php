<?php
/*
Plugin Name: FC Latest Page Redirect
Description: Redirects the home page to the latest published page, excluding the front page.
Version: 1.0
Author: Your Name
Author URI: Your Website
*/

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Redirects the home page to the latest published page, excluding the front page.
function redirect_home_to_latest_page() {
    if (is_front_page()) {
        $latest_page_args = array(
            'post_type' => 'page',
            'posts_per_page' => 1,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post__not_in' => array(get_option('page_on_front'))
        );
        $latest_page_query = new WP_Query($latest_page_args);

        if ($latest_page_query->have_posts()) {
            $latest_page_query->the_post();
            $latest_page_id = get_the_ID();
            $latest_page_url = get_permalink($latest_page_id);
            wp_reset_postdata();

            // Redirect to the page with the specified ID (e.g., 109).
            wp_redirect(get_permalink($latest_page_id), 302);
            exit;
        }
    }
}
add_action('template_redirect', 'redirect_home_to_latest_page');
