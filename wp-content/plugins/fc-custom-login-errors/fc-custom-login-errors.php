<?php
/**
 * Plugin Name: FC Custom Login Errors
 * Description: Displays a generic error message on login.
 * Version: 1.0
 * Author: Flying Cursor
 */

// Prevent direct access to this file
if (!defined('ABSPATH')) {
    exit;
}

// Ensure the plugin functionality is defined
if (!function_exists('fc_custom_login_error_message')) {
    function fc_custom_login_error_message($error) {
        return "Invalid username or password. Please try again.";
    }
}

// Hook the filter only if the function is defined
if (function_exists('fc_custom_login_error_message')) {
    add_filter('login_errors', 'fc_custom_login_error_message');
}
