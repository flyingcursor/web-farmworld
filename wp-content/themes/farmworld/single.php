<?php get_header(); ?>
<?php
$category = get_the_category(); // Get the post's categories
if (!empty($category)) {
    $category_slug = $category[0]->slug; // Get the slug of the first category
    $category_name = $category[0]->name;
}
?>

<section class="fc-post-banner pt-5 fc-<?php echo $category_slug ?>-stories">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php
                $post_banner = get_field('post_banner');
                if ($post_banner):
                    ?>
                    <div class="fc-banner-image">
                        <?php
                        echo wp_get_attachment_image($post_banner['desktop_banner'], 'full', false, array('class' => 'img-fluid d-none d-md-block'));
                        echo wp_get_attachment_image($post_banner['mobile_banner'], 'full', false, array('class' => 'img-fluid d-md-none'));
                        ?>
                    </div>
                    <?php
                endif;
                ?>
                <div class="fc-story-tag mt-4">
                    <?php
                    if ($category_name !== 'Uncategorized'):
                        ?>
                        <h4 class="story-tag"><span><?php echo $category_name; ?></span>
                            <?php echo get_field('country_name'); ?></h4>
                    <?php
                    endif;
                    ?>
                    <?php the_title('<h1 class="post-title">', '</h1>'); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="fc-post-content pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php
                if (have_posts()) {
                    // Load posts loop.
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="post-content">
                        <?php wp_kses_post(mb_convert_encoding(get_the_content(), 'UTF-8', 'auto')); ?>
                        </div>
                        <?php
                    }
                } else {

                    // If no content, include the "No posts found" template.
                
                }
                ?>
            </div>
        </div>
    </div>
</section>


<section class="fc-related-posts stories-section <?php echo $category_slug; ?>-stories">

    <div class="container">
        <div class="other-stories-section">
            <h2 class="mb-5">Other stories</h2>
        </div>

        <div class="brand-name">
            <span><?php echo $category_name; ?></span>
        </div>
        <?php
        $categories = get_the_category(); // Retrieve the categories of the current post
        
        if (function_exists('custom_get_related_posts')) {
            if ($categories) {
                $category_id = $categories[0]->cat_ID; // Get the ID of the first category
            } else {
            }
            custom_get_related_posts($category_id);
        }
        ?>
    </div>
</section>


<?php get_footer(); ?>