jQuery(document).ready(function ($) {
  $(".top-stories-slider").owlCarousel({
    items: 1,
    dots: true,
    nav: false,
    lazyLoad: true,
  });

  $(".video-stories-slider").owlCarousel({
    items: 1,
    dots: true,
    nav: false,
    lazyLoad: true,
  });

  $(".fc-story-card-slider").owlCarousel({
    items: 3,
    dots: true,
    nav: false,
    margin: 10,
    lazyLoad: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      992: {
        items: 3,
        dots: true
      }
    },
  });
});
