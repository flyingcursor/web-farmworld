<?php

/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" type="image/png" class="img-fluid" href="https://farmworld.mahindrafarmequipment.com/wp-content/uploads/2024/12/rise-favicon.png">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light fc-navbar py-4">
            <div class="container">
                <div class="d-flex flex-column">
                    <a class="navbar-brand" href="<?php echo site_url(); ?>">
                        <!-- <h1 class="h2-40">FARMWORLD</h1> -->
                        <img src="<?php echo site_url(); ?>/wp-content/uploads/2024/12/mahindra-rise.png" alt=""
                            class="img-fluid fc-mahindra-rise-logo">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/farmworld-logo.png" alt=""
                            class="img-fluid fc-farmworld-logo d-lg-none"> </a>
                    <?php if (!is_single() && get_the_ID() !== 145): ?>
                        <p class="my-2">ISSUE <?php echo get_the_date('F, Y'); ?></p>
                    <?php endif; ?>
                    <?php
                    if (is_single()) {
                        $referer = $_SERVER['HTTP_REFERER'];
                        if (!empty($referer)) {
                            // Extract the last slug from the referer URL
                            $path = parse_url($referer, PHP_URL_PATH);
                            $slug = trim(basename($path), '/');
                            $page = get_page_by_path($slug);
                            $page_created_date = get_the_date('F, Y', $page->ID);
                            ?>
                            <p class="my-2">ISSUE <?php echo $page_created_date; ?></p>

                            <?php
                        }
                    }
                    ?>

                    <!-- <p class="mb-0">ISSUE 14 / 2022</p> -->
                </div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/farmworld-logo.png" alt=""
                            class="img-fluid fc-farmworld-logo d-none d-lg-block">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end flex-lg-grow-0" id="navbarSupportedContent">
                    <ul class="navbar-nav mb-2 mb-lg-0 align-items-center">

                        <?php
                        // Check if there are any pages with the 'FC Farmworld Issue Template' template.
                        $args = array(
                            'post_type' => 'page',           // Specify the post type as 'page'.
                            'meta_key' => '_wp_page_template', // Specify the custom field key for the template.
                            'meta_value' => 'page-farmworld-issue-template.php', // Specify the template filename.
                            'posts_per_page' => -1,          // Get all matching pages.
                        );

                        $query = new WP_Query($args);

                        if ($query->post_count > 1) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link active pb-0 home-icon" aria-current="page"
                                    href="<?php echo site_url(); ?>">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/home.svg" alt=""
                                        class="img-fluid">
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-0"
                                    href="<?php echo trailingslashit(site_url()) . 'previous-issues'; ?>">Previous
                                    Issues</a>
                            </li>
                            <?php
                        }

                        // Restore the original post data.
                        wp_reset_postdata();
                        ?>
                        <?php if (is_single() && get_the_ID() !== 145): ?>
                            <li class="nav-item">
                                <a class="nav-link py-0" href="<?php echo esc_url(wp_get_referer()); ?>">Go to the previous
                                    issue</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
    </header>