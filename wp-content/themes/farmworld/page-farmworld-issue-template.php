<?php

/**
 * Template Name: FC Farmworld Issue Template
 */

global $post;
$slug = $post->post_name;

get_header(); ?>

<!-- Main Banner Section starts -->
<section class="main-banner pt-2 py-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 my-2">
                <div class="top-stories-section position-relative story-snipppet">
                    <div class="headline">
                        <p class="mb-0">Top stories</p>
                    </div>
                    <?php
                    if (have_rows('stories')):
                        ?>
                        <div class="top-stories top-stories-slider owl-carousel">
                            <?php
                            while (have_rows('stories')):
                                the_row();
                                ?>
                                <div class="fc-story">
                                    <?php
                                    echo wp_get_attachment_image(get_sub_field('image'), 'full', false, array('class' => 'img-fluid'));
                                    ?>
                                    <div class="img-overlay"></div>
                                    <div class="story-content">
                                        <?php echo get_sub_field('content'); ?>
                                        <a href="<?php echo get_sub_field('link'); ?>" class="stretched-link">read more</a>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                            ?>
                        </div>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
            <div class="col-12 col-lg-6 my-2">
                <div class="row">
                    <?php
                    if (empty(get_field('show_only_video_section'))):
                        $spacial_feature = get_field('spacial_feature');
                        if ($spacial_feature):
                            ?>
                            <div class="col-12 col-lg-6 my-2 my-lg-0">
                                <div class="story-snipppet-sm position-relative">
                                    <div class="headline">
                                        <p class="mb-0">A charismatic beginning</p>
                                    </div>
                                    <div class="feature-story">
                                        <?php
                                        echo wp_get_attachment_image($spacial_feature['image'], 'full', false, array('class' => 'img-fluid'));
                                        ?>
                                        <div class="story-content-sm">
                                            <h4><?php echo $spacial_feature['title']; ?></h4>
                                            <a href="<?php echo $spacial_feature['link']; ?>" class="stretched-link">read
                                                more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endif;
                        ?>
                        <?php
                        if (have_rows('face_to_face')):
                            ?>
                            <div class="col-12 col-lg-6 my-2 my-lg-0">
                                <div class="video-story-snippet position-relative">
                                    <div class="headline">
                                        <p class="mb-0">Face to face</p>
                                    </div>

                                    <div class="video-stories-slider owl-carousel">
                                        <?php
                                        while (have_rows('face_to_face')):
                                            the_row();
                                            ?>
                                            <div class="video-story position-relative">
                                                <?php
                                                // Display the image
                                                echo wp_get_attachment_image(get_sub_field('image'), 'full', false, array('class' => 'img-fluid'));
                                                ?>

                                                <div class="img-overlay"></div>
                                                <div class="video-text-content">
                                                    <?php
                                                    // Get the content and link fields
                                                    echo get_sub_field('content');
                                                    $link = get_sub_field('link'); // Link array from ACF
                                        
                                                    if ($link) {
                                                        // Check if the URL contains 'youtube.com' or 'youtu.be'
                                                        $url = $link['url'];
                                                        $label = $link['title']; // Optional: Use ACF link label
                                        
                                                        if (strpos($url, 'youtube.com') !== false || strpos($url, 'youtu.be') !== false) {
                                                            // If it's a YouTube link, show the popup link with "watch now"
                                                            echo '<a href="' . esc_url($url) . '" class="fc-video-popup-link stretched-link">watch now <i class="fa-regular fa-circle-play"></i></a>';
                                                        } else {
                                                            // For non-YouTube links, show the regular link with the given label
                                                            echo '<a href="' . esc_url($url) . '" class="stretched-link">' . esc_html($label) . ' <i class="fa-regular fa-circle-play"></i></a>';
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                        <?php endwhile; ?>
                                    </div>
                                </div>

                            </div>
                            <?php
                        endif;
                    else:
                        if (have_rows('video_section_1')):
                            ?>
                            <div class="col-12 col-lg-6 my-2 my-lg-0">
                                <div class="video-story-snippet position-relative">
                                    <div class="headline">
                                        <p class="mb-0"><?php the_field('video_section_1_-_heading') ?></p>
                                    </div>

                                    <div class="video-stories-slider owl-carousel">
                                        <?php
                                        while (have_rows('video_section_1')):
                                            the_row();
                                            ?>
                                            <div class="video-story position-relative">
                                                <?php
                                                // Display the image
                                                echo wp_get_attachment_image(get_sub_field('image'), 'full', false, array('class' => 'img-fluid'));
                                                ?>

                                                <div class="img-overlay"></div>
                                                <div class="video-text-content">
                                                    <?php
                                                    // Get the content and link fields
                                                    echo get_sub_field('content');
                                                    $link = get_sub_field('link'); // Link array from ACF
                                        
                                                    if ($link) {
                                                        // Check if the URL contains 'youtube.com' or 'youtu.be'
                                                        $url = $link['url'];
                                                        $label = $link['title']; // Optional: Use ACF link label
                                        
                                                        if (strpos($url, 'youtube.com') !== false || strpos($url, 'youtu.be') !== false) {
                                                            // If it's a YouTube link, show the popup link with "watch now"
                                                            echo '<a href="' . esc_url($url) . '" class="fc-video-popup-link stretched-link">watch now <i class="fa-regular fa-circle-play"></i></a>';
                                                        } else {
                                                            // For non-YouTube links, show the regular link with the given label
                                                            echo '<a href="' . esc_url($url) . '" class="stretched-link">' . esc_html($label) . ' <i class="fa-regular fa-circle-play"></i></a>';
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endif;
                        if (have_rows('video_section_2')):
                            ?>
                            <div class="col-12 col-lg-6 my-2 my-lg-0">
                                <div class="video-story-snippet position-relative">
                                    <div class="headline">
                                        <p class="mb-0"><?php the_field('video_section_2_-_heading') ?></p>
                                    </div>

                                    <div class="video-stories-slider owl-carousel">
                                        <?php
                                        while (have_rows('video_section_2')):
                                            the_row();
                                            ?>
                                            <div class="video-story position-relative">
                                                <?php
                                                // Display the image
                                                echo wp_get_attachment_image(get_sub_field('image'), 'full', false, array('class' => 'img-fluid'));
                                                ?>

                                                <div class="img-overlay"></div>
                                                <div class="video-text-content">
                                                    <?php
                                                    // Get the content and link fields
                                                    echo get_sub_field('content');
                                                    $link = get_sub_field('link'); // Link array from ACF
                                        
                                                    if ($link) {
                                                        // Check if the URL contains 'youtube.com' or 'youtu.be'
                                                        $url = $link['url'];
                                                        $label = $link['title']; // Optional: Use ACF link label
                                        
                                                        if (strpos($url, 'youtube.com') !== false || strpos($url, 'youtu.be') !== false) {
                                                            // If it's a YouTube link, show the popup link with "watch now"
                                                            echo '<a href="' . esc_url($url) . '" class="fc-video-popup-link stretched-link">watch now <i class="fa-regular fa-circle-play"></i></a>';
                                                        } else {
                                                            // For non-YouTube links, show the regular link with the given label
                                                            echo '<a href="' . esc_url($url) . '" class="stretched-link">' . esc_html($label) . ' <i class="fa-regular fa-circle-play"></i></a>';
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>

                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endif;
                    endif;
                    ?>
                </div>
                <div class="row my-4">
                    <?php
                    $farm_story = get_field('farm_story');
                    if ($farm_story && $farm_story['title']):
                        ?>
                        <div class="col-12 col-lg-6 my-2 my-lg-0">
                            <div class="story-snipppet-sm position-relative">
                                <div class="headline">
                                    <p class="mb-0">Farm Story</p>
                                </div>
                                <div class="feature-story">
                                    <?php
                                    echo wp_get_attachment_image($farm_story['image'], 'full', false, array('class' => 'img-fluid'));
                                    ?>
                                    <div class="story-content-sm">
                                        <h4><?php echo $farm_story['title']; ?></h4>
                                        <a href="<?php echo $farm_story['link']; ?>" class="stretched-link">read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    endif;
                    ?>

                </div>
            </div>
            <?php
            $is_multiple_newletters = get_field('is_multiple_newletters');
            $article_content = get_field('article_content');
            $type = get_field('type');

            if ($is_multiple_newletters):
                if (have_rows('newletters')):
                    while (have_rows('newletters')):
                        the_row();
                        ?>
                        <div class="col-12 col-lg-6">
                            <div class="story-snipppet position-relative">
                                <div class="headline">
                                    <p class="mb-0 text-uppercase"><?php echo get_sub_field('headline'); ?></p>
                                </div>
                                <div class="article-video-section">
                                    <?php
                                    if ($type == 'article'):
                                        ?>
                                        <div class="row fc-letter gx-0">
                                            <div class="col-12 col-lg-4">
                                                <?php
                                                echo wp_get_attachment_image(get_sub_field('image'), 'full', false, array('class' => 'img-fluid'));
                                                ?>
                                            </div>
                                            <div class="col-12 col-lg-8 message-scroll">
                                                <?php echo get_sub_field('message'); ?>
                                            </div>
                                        </div>
                                        <?php
                                    else:
                                        ?>
                                        <iframe src="<?php echo get_field('video_link'); ?>" allowfullscreen></iframe>
                                        <?php
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
            else:

                if ($article_content['headline']):
                    ?>
                    <div class="col-12 my-2">
                        <div class="story-snipppet position-relative">
                            <div class="headline">
                                <p class="mb-0 text-uppercase"><?php echo $article_content['headline']; ?></p>
                            </div>
                            <div class="article-video-section">
                                <?php
                                if ($type == 'article' && $article_content):
                                    ?>
                                    <div class="row fc-letter gx-0">
                                        <div class="col-12 col-lg-4">
                                            <?php
                                            echo wp_get_attachment_image($article_content['image'], 'full', false, array('class' => 'img-fluid'));
                                            ?>
                                        </div>
                                        <div class="col-12 col-lg-8 message-scroll">
                                            <?php echo $article_content['message']; ?>
                                        </div>
                                    </div>
                                    <?php
                                else:
                                    ?>
                                    <iframe src="<?php echo get_field('video_link'); ?>" allowfullscreen></iframe>
                                    <?php
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                endif;
            endif;

            ?>
        </div>
    </div>
</section>
<!-- Main banner section ends -->

<section class="other-stories-section py-4">
    <div class="container">
        <h2>Other stories</h2>
    </div>
</section>


<!-- Stories section starts -->
<?php
$args = array(
    'post_type' => 'post',
    'cat' => '4',
    'posts_per_page' => -1,
    'meta_key' => 'country_name', // Assuming 'country_name' is the custom field key
    'orderby' => 'meta_value',   // Order by the value of the custom field
    'tag' => $slug, // Assuming 'your-tag-slug' is the tag slug
    'order' => 'ASC',       // Order in ascending alphabetical order
    'meta_query' => array(
        array(
            'key' => 'feature_post',
            'value' => '1', // Show posts with 'feature_post' set to false (0)
            'compare' => '!='
        )
    )
);

$lastposts_query = new WP_Query($args);

if ($lastposts_query->have_posts()):
    ?>
    <section class="stories-section mahindra-stories">
        <div class="container">
            <div class="brand-name">
                <span><?php echo get_the_category_by_ID('4'); ?></span>
            </div>

            <div class="row my-5 owl-carousel fc-story-card-slider">
                <?php
                while ($lastposts_query->have_posts()):
                    $lastposts_query->the_post();
                    $country_name = get_field('country_name', get_the_ID());
                    ?>
                    <div class="col-12 col-lg-4 w-100">
                        <div class="card fc-story-card">
                            <div class="image-thumbnail">
                                <?php
                                if (has_post_thumbnail()) {
                                    ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                        the_post_thumbnail('full', array('class' => 'img-fluid w-100'));
                                        ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="fc-story-tag mt-4">
                                <h4><span><?php echo get_the_category_by_ID('4'); ?></span> <?php echo $country_name; ?></h4>
                            </div>
                            <div class="card-body">
                                <?php the_title('<h5 class="card-title">', '</h5>'); ?>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="stretched-link">read more</a>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </section>
    <?php
endif;
?>


<?php
$lastposts = get_posts(
    array(
        'post_type' => 'post',
        'cat' => '7',
        'posts_per_page' => -1,
        'meta_key' => 'country_name', // Assuming 'country_name' is the custom field key
        'tag' => $slug, // Assuming 'your-tag-slug' is the tag slug
        'orderby' => 'meta_value',   // Order by the value of the custom field
        'order' => 'ASC',       // Order in ascending alphabetical order
        'meta_query' => array(
            array(
                'key' => 'feature_post',
                'value' => '1', // Show posts with 'feature_post' set to false (0)
                'compare' => '!='
            )
        )
    )
);

if ($lastposts):
    ?>
    <section class="py-3 stories-section swaraj-stories">
        <div class="container">
            <div class="brand-name">
                <span><?php echo get_the_category_by_ID('7'); ?></span>
            </div>
            <div class="row my-5 owl-carousel fc-story-card-slider">
                <?php
                foreach ($lastposts as $post):
                    setup_postdata($post);
                    $country_name = get_field('country_name', $post->ID);
                    ?>
                    <div class="col-12 col-lg-4 w-100">
                        <div class="card fc-story-card">
                            <div class="image-thumbnail">
                                <?php
                                if (has_post_thumbnail()) {
                                    ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                        the_post_thumbnail('full', array('class' => 'img-fluid w-100'));
                                        ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="fc-story-tag mt-4">
                                <h4><span><?php echo get_the_category_by_ID('7'); ?></span> <?php echo $country_name; ?></h4>
                            </div>
                            <div class="card-body">
                                <?php the_title('<h5 class="card-title">', '</h5>'); ?>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="stretched-link">read more</a>
                            </div>
                        </div>
                    </div>
                    <?php
                endforeach;
                ?>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata();
endif;
?>

<?php
$lastposts = get_posts(
    array(
        'post_type' => 'post',
        'cat' => '3',
        'posts_per_page' => -1,
        'meta_key' => 'country_name', // Assuming 'country_name' is the custom field key
        'tag' => $slug, // Assuming 'your-tag-slug' is the tag slug
        'orderby' => 'meta_value',   // Order by the value of the custom field
        'order' => 'ASC',       // Order in ascending alphabetical order
        'meta_query' => array(
            array(
                'key' => 'feature_post',
                'value' => '1', // Show posts with 'feature_post' set to false (0)
                'compare' => '!='
            )
        )
    )
);

if ($lastposts):
    ?>
    <section class="py-3 stories-section erkunt-stories">
        <div class="container">
            <div class="brand-name">
                <span><?php echo get_the_category_by_ID('3'); ?></span>
            </div>

            <div class="row my-5 owl-carousel fc-story-card-slider">
                <?php
                foreach ($lastposts as $post):
                    setup_postdata($post);
                    $country_name = get_field('country_name', $post->ID);
                    ?>
                    <div class="col-12 col-lg-4 w-100">
                        <div class="card fc-story-card">
                            <div class="image-thumbnail">
                                <?php
                                if (has_post_thumbnail()) {
                                    ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                        the_post_thumbnail('full', array('class' => 'img-fluid w-100'));
                                        ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="fc-story-tag mt-4">
                                <h4><span><?php echo get_the_category_by_ID('3'); ?></span> <?php echo $country_name; ?></h4>
                            </div>
                            <div class="card-body">
                                <?php the_title('<h5 class="card-title">', '</h5>'); ?>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="stretched-link">read more</a>
                            </div>
                        </div>
                    </div>
                    <?php
                endforeach;
                ?>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata();
endif;
?>

<?php
$lastposts = get_posts(
    array(
        'post_type' => 'post',
        'cat' => '5',
        'posts_per_page' => 10,
        'meta_key' => 'country_name', // Assuming 'country_name' is the custom field key
        'tag' => $slug, // Assuming 'your-tag-slug' is the tag slug
        'orderby' => 'meta_value',   // Order by the value of the custom field
        'order' => 'ASC',       // Order in ascending alphabetical order
        'meta_query' => array(
            array(
                'key' => 'feature_post',
                'value' => '1', // Show posts with 'feature_post' set to false (0)
                'compare' => '!='
            )
        )
    )
);

if ($lastposts):
    ?>
    <section class="py-3 stories-section mitsubishi-stories">
        <div class="container">
            <div class="brand-name">
                <span><?php echo get_the_category_by_ID('5'); ?></span>
            </div>

            <div class="row my-5 owl-carousel fc-story-card-slider">
                <?php
                foreach ($lastposts as $post):
                    setup_postdata($post);
                    $country_name = get_field('country_name', $post->ID);
                    ?>
                    <div class="col-12 col-lg-4 w-100">
                        <div class="card fc-story-card">
                            <div class="image-thumbnail">
                                <?php
                                if (has_post_thumbnail()) {
                                    ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                        the_post_thumbnail('full', array('class' => 'img-fluid w-100'));
                                        ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="fc-story-tag mt-4">
                                <h4 class="text-uppercase"><span><?php echo get_the_category_by_ID('5'); ?></span>
                                    <?php echo $country_name; ?></h4>
                            </div>
                            <div class="card-body">
                                <?php the_title('<h5 class="card-title">', '</h5>'); ?>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="stretched-link">read more</a>
                            </div>
                        </div>
                    </div>
                    <?php
                endforeach;
                ?>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata();
endif;
?>

<?php
$lastposts = get_posts(
    array(
        'post_type' => 'post',
        'cat' => '6',
        'posts_per_page' => -1,
        'meta_key' => 'country_name', // Assuming 'country_name' is the custom field key
        'tag' => $slug, // Assuming 'your-tag-slug' is the tag slug
        'orderby' => 'meta_value',   // Order by the value of the custom field
        'order' => 'ASC',       // Order in ascending alphabetical order
        'meta_query' => array(
            array(
                'key' => 'feature_post',
                'value' => '1', // Show posts with 'feature_post' set to false (0)
                'compare' => '!='
            )
        )
    )
);

if ($lastposts):
    ?>
    <section class="py-3 stories-section sampo-rosenlew-stories">
        <div class="container">
            <div class="brand-name">
                <span><?php echo get_the_category_by_ID('6'); ?></span>
            </div>

            <div class="row my-5 owl-carousel fc-story-card-slider">
                <?php
                foreach ($lastposts as $post):
                    setup_postdata($post);
                    $country_name = get_field('country_name', $post->ID);
                    ?>
                    <div class="col-12 col-lg-4 w-100">
                        <div class="card fc-story-card">
                            <div class="image-thumbnail">
                                <?php
                                if (has_post_thumbnail()) {
                                    ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                        the_post_thumbnail('full', array('class' => 'img-fluid w-100'));
                                        ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="fc-story-tag mt-4">
                                <h4><span><?php echo get_the_category_by_ID('6'); ?></span> <?php echo $country_name; ?></h4>
                            </div>
                            <div class="card-body">
                                <?php the_title('<h5 class="card-title">', '</h5>'); ?>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="stretched-link">read</a>
                            </div>
                        </div>
                    </div>
                    <?php
                endforeach;
                ?>
            </div>
        </div>
    </section>
    <?php
    wp_reset_postdata();
endif;
?>

<section>
    <div class="container">
        <div>
            <div class="fc-share-news">
                <div class="py-3">
                    <h3>Share Your news!</h3>
                    <p>Share all the latest events,
                        news of our business partners
                        and associates and
                        their stories that touch a chord.</p>
                    <a href="mailto:mfarmworld@mahindra.com">share now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Stories section ends -->


<?php get_footer(); ?>