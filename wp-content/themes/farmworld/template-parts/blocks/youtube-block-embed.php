<?php

// Support custom "anchor" values.
$anchor = '';
if (!empty($args['anchor'])) {
    $anchor = 'id="' . esc_attr($args['anchor']) . '" ';
}

// Create class attribute allowing for custom "className" and "align" values.
$class_name = '';
if (!empty($args['className'])) {
    $class_name .= ' ' . $args['className'];
}
if (!empty($args['align'])) {
    $class_name .= ' align' . $args['align'];
}

?>
<section <?php echo $anchor; ?> class="<?php echo esc_attr($class_name); ?>">
    <div class="container">
        <div class="ratio ratio-16x9">
            <iframe src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" title="YouTube video" allowfullscreen></iframe>
        </div>
    </div>
</section>