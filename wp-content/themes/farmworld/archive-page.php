<?php
/*
Template Name: Archive Page
*/

get_header();
?>

<section class="fc-archieve-pages py-5">
    <div class="container">
        <div class="accordion fc-archieve-accordion" id="archiveAccordion">
            <?php
            $args = array(
                'post_type' => 'page',
                'posts_per_page' => -1, // Display all pages
                'meta_query' => array(
                    array(
                        'key' => '_wp_page_template',
                        'value' => 'page-farmworld-issue-template.php', // Replace with the actual filename of the template
                    ),
                ),
            );

            $query = new WP_Query($args);
            $years = array();

            if ($query->have_posts()) :
                while ($query->have_posts()) : $query->the_post();
                    $year = get_the_time('Y');
                    if (!in_array($year, $years)) {
                        $years[] = $year;
                    }
                endwhile;
                wp_reset_postdata();

                // Sort years in descending order
                rsort($years);
                $firstAccordion = true; // Flag to track the first accordion item

                foreach ($years as $year) :
            ?>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading<?php echo $year; ?>">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $year; ?>" aria-expanded="true" aria-controls="collapse<?php echo $year; ?>">
                                <?php echo $year; ?>
                            </button>
                        </h2>
                        <div id="collapse<?php echo $year; ?>" class="accordion-collapse collapse <?php if ($firstAccordion) echo 'show'; ?>"" aria-labelledby="heading<?php echo $year; ?>" data-bs-parent="#archiveAccordion">
                            <div class="accordion-body">
                                <ul>
                                    <?php
                                    $args = array(
                                        'post_type' => 'page',
                                        'posts_per_page' => -1,
                                        'date_query' => array(
                                            array(
                                                'year' => $year,
                                            ),
                                        ),
                                        'meta_query' => array(
                                            array(
                                                'key' => '_wp_page_template',
                                                'value' => 'page-farmworld-issue-template.php', // Replace with the actual filename of the template
                                            ),
                                        ),
                                    );

                                    $query = new WP_Query($args);

                                    if ($query->have_posts()) :
                                        while ($query->have_posts()) : $query->the_post();
                                    ?>
                                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                    <?php
                                        $firstAccordion = false;
                                        endwhile;
                                    else :
                                        echo '<li>No pages found for this year.</li>';
                                    endif;

                                    wp_reset_postdata();
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
            <?php
                endforeach;
            else :
                echo '<p>No pages found.</p>';
            endif;
            ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>