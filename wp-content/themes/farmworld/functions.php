<?php

/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */


/**
 * Enqueue custom style sheet
 *
 * @return void
 */
function load_stylesheet()
{
  $cssArr = [
    'fc-default' => '/assets/css/default.css',
    'owl-carousel' => '/assets/css/owl.carousel.min.css',
    'fc-carousel-styles' => '/assets/css/fc-carousel.css',
    'magnific-popup' => '/assets/css/magnific-popup.css'
  ];
  $enqueCss = [];

  foreach ($cssArr as $key => $value) {
    wp_register_style($key, get_stylesheet_directory_uri() . $value, array(), filemtime(get_stylesheet_directory() . $value));
    $enqueCss[] = $key;
  }
  wp_register_style('styles', get_stylesheet_directory_uri() . '/assets/css/styles.css', $enqueCss, filemtime(get_stylesheet_directory() . '/assets/css/styles.css'), 'all');
  wp_enqueue_style('styles');
}
add_action('wp_enqueue_scripts', 'load_stylesheet', 50);

add_theme_support('post-thumbnails');

/**
 * Enqueue Custom Script
 *
 * @return void
 */
function load_scripts()
{
  $jsArr = [
    'owl-carousel' => '/assets/js/owl.carousel.min.js',
    'fc-carousel' => '/assets/js/fc-carousel.js',
    'magnific-popup-min' => '/assets/js/jquery.magnific-popup.min.js',
    'magnific-popup' => '/assets/js/jquery.magnific-popup.js',

  ];
  $enqueJs = ['jquery'];

  foreach ($jsArr as $key => $value) {
    wp_register_script($key, get_stylesheet_directory_uri() . $value, array(), filemtime(get_stylesheet_directory() . $value));
    $enqueJs[] = $key;
  }

  wp_register_script('init', get_stylesheet_directory_uri() . '/assets/js/init.js', $enqueJs, filemtime(get_stylesheet_directory() . '/assets/js/init.js'), true);

  wp_enqueue_script('init');
}
add_action('wp_enqueue_scripts', 'load_scripts');

function disable_all_feeds()
{
  wp_die(__('No feeds available, please visit our homepage.'));
}

add_action('do_feed', 'disable_all_feeds', 1);
add_action('do_feed_rdf', 'disable_all_feeds', 1);
add_action('do_feed_rss', 'disable_all_feeds', 1);
add_action('do_feed_rss2', 'disable_all_feeds', 1);
add_action('do_feed_atom', 'disable_all_feeds', 1);

function disable_author_archive()
{
  if (is_author()) {
    wp_redirect(home_url());
    exit;
  }
}
add_action('template_redirect', 'disable_author_archive');

/**
 * Remove users from sitemap.
 */
function exclude_users_from_sitemap($post_types)
{
  // Add 'user' to the list of post types to exclude
  $post_types[] = 'user';
  return $post_types;
}
add_filter('wpseo_sitemap_entry', 'exclude_users_from_sitemap');

/**
 * Disable Plugin Uploads
 */
define('DISALLOW_FILE_MODS', true);

/** Restrict to only image and pdf */
function custom_upload_mimes($existing_mimes = array())
{
  // Add here the mime types you want to allow
  $allowed = array(
    'jpg|jpeg|jpe' => 'image/jpeg',
    'gif' => 'image/gif',
    'png' => 'image/png',
    'bmp' => 'image/bmp',
    'tif|tiff' => 'image/tiff',
    'pdf' => 'application/pdf',
    'mp4|m4v' => 'video/mp4', // Add video mime types here
    'mov|qt' => 'video/quicktime',
    'wmv' => 'video/x-ms-wmv',
    'webm' => 'video/webm',
  );

  return $allowed;
}

add_filter('upload_mimes', 'custom_upload_mimes');

// Add wordpress title support
add_theme_support('title-tag');

// Fix encoding issue
function fix_the_content_encoding($content) {
  // Convert the content to UTF-8
  return mb_convert_encoding($content, 'UTF-8', 'auto');
}
add_filter('the_content', 'fix_the_content_encoding', 50);

function add_class_to_excerpt($excerpt)
{
  return str_replace('<p', '<p class="card-text"', mb_convert_encoding($excerpt,'UTF-8', 'auto'));
}
add_filter('the_excerpt', 'add_class_to_excerpt');